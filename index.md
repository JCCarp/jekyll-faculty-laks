---
layout: default
---

## Quickstart Guide

Click [here](https://bitbucket.org/UBCCS/jekyll-faculty/src/master/) to return to the 
repository website that shows instructions on how to use
and customize this UBC CS faculty and course static website generator yourself.  Browse
the markdown files in particular (```.md``` file extension; start with ```index.md``` -- make sure you view the raw source).

Browse the remainder of this demonstration website to gain a sense of what can
be easily and efficiently generated and maintained (like $x^2 + 1 = 17$, ```while(true) ++i```, and the references below).

---------------
## About Me

<img class="profile-picture" src="headshot.jpg">

I am a professor of computer science at the University of British Columbia.


## Research Interests

As the world we live in is getting more and more networked, the need to understand, manage, and harness the data on the web is becoming critical. While data in traditional databases tends to be highly structured, with a clear notion of schema, data on the web is loosely structured (also called semi-structured), or worse, unstructured, and is often not accompained by any clear notion of schema. What does it mean to query this data? What do you look for when you mine this data? If there are several data sources containing related information, how do you combine the information in them to answer queries involving them all? How can you index such data for efficient storage and retrieval? What do you do when the data you want to analyze is not stored some place but is streaming through? My research over the past few years has been concerned with addressing these questions. I am also interested in newer applications which challenge the foundations and technology of databases. A good example is bioinformatics.

## Selected Publications

{% bibliography --file selected.bib %}
