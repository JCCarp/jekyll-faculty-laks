---
title: Papers
layout: default
permalink: /papers/
---

# Google Scholar
[Recent First](https://scholar.google.ca/citations?hl=en&user=_RCsaOsAAAAJ&view_op=list_works&sortby=pubdate), [Most Cited First](https://scholar.google.ca/citations?user=_RCsaOsAAAAJ&hl=en)

# Conference
{% bibliography --file conference %}

# Journal
{% bibliography --file journal %}

# Technical Reports
{% bibliography --file techreportandarxiv %}

# Book Chapters
{% bibliography --file chapters %}

# Invited Papers
{% bibliography --file invited %}

# Workshop Publications
{% bibliography --file workshop %}
